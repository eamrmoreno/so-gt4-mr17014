#include <stdio.h>

//Metodo que se utilizara para imprimir el puntero
void imprimir(int *a){
  //Se hace la sumatoria
  *a = *a + 1;
  printf("%d\n", *a);
}

//Metodo principal
int main() {
  int i, a=0;
  //Se va iterando hasta llegar a 5
  for (i = 0; i < 5; i++) {
    //Se llama al metodo que vamos a ocupar para imprimir
    imprimir(&a);
  }

  return 0;
}
