#include <stdio.h>

//Metodo principal del programa
int main(){
  int *pnumero;
  int num1, num2;
  char *pchar;
  char letra1;
  num1 = 2;
  num2 = 5;
  letra1 = 'a';

  //Se asigna al puntero el valor que contiene num1
  pnumero = &num1;
  printf("pnumero tiene el valor de %d\n",*pnumero );

return 0;
}
