#include <stdio.h>

//Metodo que se ocupara para imprimir pidiendo un valor de tipo int
void imprimir(int a) {
  a = a+1;
  printf("%d\n", a);
}

//Metodo principal que se ocupara en el programa
int main() {
  int i, a = 0;
  //Se hace la iteracion hasta llegar a 7
  for (i = 0; i < 7; i++) {
    //Se llama al metodo imprimir que se utilizara para imprimir
    imprimir(a);
  }
  return 0;
}
