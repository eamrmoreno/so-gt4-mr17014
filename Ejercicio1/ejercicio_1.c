#include <stdio.h>

//Metodo principal
int main() {

  float n1;
  float n2;
  float *p1;
  float *p2;
  n1 = 4.0;
  p1 = &n1;
  p2=p1;
  n2= *p2;
  n1 = *p1 + *p2;
  //Se imprime las cantidades de n1 y n2
  printf(" los resultados son:\nn1 = %.2f\nn2 = %.2f\n",n1,n2);

  return 0;
}
