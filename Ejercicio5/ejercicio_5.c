#include <stdio.h>
#include <stdlib.h>
#define TOPE 101

void burbuja(int *x, int *y);
void ordenar(int numer[], int n);
void mostrar(int numer[], int n);

int main() {

  int *p_numeros;
  int i;
  //apuntamos hacia un espacio de memoria de 20 veces el tamaño de un int de 4 bytes
  p_numeros = (int *)malloc(20*sizeof(int));
  //verificamos si hay memoria disponible
  if (p_numeros == NULL) {
    puts("Error en la asignacion de memoria");
    return -1;
  }
  //llenamos el array con numeros aleatoreos entre 0 y 100
  for (i = 0; i < 20; i++) {
    p_numeros[i] = (int)(rand() % 101);
  }

  ordenar(p_numeros,20);
  mostrar(p_numeros, 20);
  free(p_numeros);
  return 0;

}

/*
funcion que contiene metodo de la burbuja
*/
void burbuja(int *x, int *y){
  int bur;
  bur = *x;
  *x = *y;
  *y = bur;
}

/*
funcion para ordenar el array de menor a mayor haciendo el llamado al metodo de la burbuja
*/
void ordenar(int numer[], int n){

  for (int i = n-1; i > 0; i--) {
    for (int j = 0; j < i; j++) {
      if (numer[j] > numer[j+1]) {
        burbuja(&numer[j], &numer[j+1]);
      }
    }
  }
}
/*
funcion para imprimir el arrar de mayor a menor
*/
  void mostrar(int numer[], int n){
    int i;
    printf("Los numeros aleatoreos ordenados del array son:\n");
    for (i = n-1; i >= 0; i--) {
      printf("%d\n", numer[i] );
    }
  }
